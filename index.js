/*
    AJAX - це технологія, за допомогою якої можна асинхронно обмінюватися данними з сервером без перезавантаження сторінки (у фоновому режимі). Це дозволяє отримати більш продуктивний веб-додаток.
*/

const filmsList = document.querySelector('#films-list')

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
        const films = data
        
        films.forEach((film) => {
            const filmItem = document.createElement('li')
            
            filmItem.insertAdjacentHTML(
                'beforeend',
                `<p> <b>EpisodeId - </b>${film.episodeId}, </p>
                 <p id="filmName"> <b>Name - </b>"${film.name}"</p>
                 <p> <b>OpeningCrawl - </b>${film.openingCrawl}</p>`
                )
                
            const filmName = filmItem.querySelector('#filmName')

            const charactersTitle = document.createElement('p')
            charactersTitle.classList.add('filmCharacters')
            charactersTitle.innerHTML = '<b>Characters :</b>'
            
            anotherUrls(film.characters)
            .then(data => {
                const charactersNames = data.map(character => character.name)
                
                const charactersList = document.createElement('ul')
                charactersList.classList.add('charactersList')
                charactersList.innerHTML = charactersNames.map(name => `<li>${name}</li>`).join('')

                charactersTitle.append(charactersList)
                filmName.after(charactersTitle)
            })

            filmsList.append(filmItem)
        })
    })
    .catch(err => console.warn(err))


function anotherUrls(urls) {
    const requests = urls.map(url => fetch(url)
        .then(data => data.json()))
    return Promise.all(requests)
}
